import 'dart:async';

import 'package:floor/floor.dart';
import 'package:graduationproject/dao/NoteDao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:path/path.dart';
import 'entity/Note.dart';
part 'database.g.dart';

@Database(version: 1, entities: [Note])
abstract class AppDatabase extends FloorDatabase {
  NoteDao get note;
}