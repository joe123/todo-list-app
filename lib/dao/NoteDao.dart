// TODO Implement this library.
import 'package:floor/floor.dart';
import 'package:graduationproject/entity/Note.dart';

@dao
abstract class NoteDao {
  @Query('SELECT * FROM Note')
  Future<List<Note>> findAllNotes();

  @Query('SELECT * FROM Note WHERE id = :id')
  Stream<Note> findNoteById(int id);

  @insert
  Future<void> insertNote(Note note);

  @update
  Future<int> updateNote(Note note);

  @delete
  Future<int> deleteNote(Note note);
}