// TODO Implement this library.
import 'package:floor/floor.dart';

@entity
class Note {
  @PrimaryKey(autoGenerate: true)
   int id;
   bool done;
   String noteTitle;
   String description;

  Note(this.id,this.done, this.noteTitle, this.description);
}
