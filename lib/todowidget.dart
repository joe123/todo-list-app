import 'package:flutter/material.dart';
import 'package:floor/floor.dart';
import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'dao/NoteDao.dart';
import 'database.dart';
import 'entity/Note.dart';

void main() {
  runApp(TodoWidget());
}


class TodoWidget extends StatefulWidget {
  Note note;

  TodoWidget({Key key, this.note}) : super(key: key);

  @override
  _TodoWidgetState createState() => _TodoWidgetState();
// This widget is the root of your application.


}

class _TodoWidgetState extends State<TodoWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text('TO DO list'),
            backgroundColor: Colors.blue,
          ),
          body: Center(
            child: Column(

              children: <Widget>[
                FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(9),
                  splashColor: Colors.blueAccent,


                  onPressed: () async {
                    final database = await $FloorAppDatabase.databaseBuilder(
                        'app_database.db').build();
                    final noteDao = database.note;

                    // final note = Note(
                    //      false, "Note title", 'Note descriptions');
                    if (widget.note.id == 1) {
                      widget.note.id =
                          new DateTime.now().millisecondsSinceEpoch;
                      await noteDao.insertNote(widget.note);
                    }
                    else {
                      await noteDao.updateNote(widget.note);
                    }

                    // var test=await noteDao.findAllNotes();
                    Navigator.of(context).pop(widget.note);
                  },
                  child: Text(
                    "Save",
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(
                    initialValue: widget.note.noteTitle,
                    onChanged: (text) {
                      widget.note.noteTitle = text;
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Note title',
                      hintText: ' Enter Note title',
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(maxLines: 7,
                    initialValue: widget.note.description,
                    onChanged: (text) {
                      widget.note.description = text;
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Note descriptions',
                      hintText: 'Enter note description ',

                    ),
                  ),
                ),

              ],
            ),
          ),
        )
    );
  }
}


