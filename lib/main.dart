import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:graduationproject/dao/NoteDao.dart';
import 'package:graduationproject/todowidget.dart';

import 'database.dart';
import 'entity/Note.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo List',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
        brightness: Brightness.light,
        primaryColor: Colors.pink,
      ),
      home: MyHomePage(title: 'Todo List'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Note> todolist = new List<Note>();
  bool valuefirst = false;
  NoteDao noteDao;
  var database;

  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_)async {
      await refreshDatebase();
    }
    );


    // todolist.add(new todos("Task1"," Meeting", valuefirst));
    // todolist.add(new todos("Task2"," Check Mails", valuefirst));
    // todolist.add(new todos("Task3"," Review Drawing", valuefirst));
    // todolist.add("Task4");
  }

  Future refreshDatebase() async {
        database = await $FloorAppDatabase
        .databaseBuilder('app_database.db')
        .build();
    noteDao = database.note;
    List<Note> notes = await noteDao.findAllNotes();
        todolist.clear();
        setState(() {
          todolist.addAll(notes);
        });

    if (todolist == null)
      todolist = new List<Note>();
  }

  String input = "";

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('Todo List',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
                color: Colors.white)),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          navigateToPage(new Note(1,false,"", ""),context);
          // showDialog(
          //     context: context,
          //     builder: (BuildContext context) {
          //   return AlertDialog(
          //     title: Text('Add ToDo Note Title'),
          //     content: TextField(
          //       //Called when the user initiates a change to the TextField's value: when they have inserted or deleted text.
          //         onChanged: (String value) {
          //           input = value;
          //         },
          //     ),
          //     actions: <Widget>[
          //       FlatButton(onPressed: () {
          //         setState(() {
          //           todos.add(input);
          //         });
          //       },
          //           child: Text("Add"))
          //     ],
          //
          //   );
          // });
        },
        child: Icon(Icons.add, color: Colors.white),
      ),
      body: ListView.builder(
          itemCount: todolist.length,
          itemBuilder: (BuildContext context, int index) {
            final item = todolist[index];
            //Displays a Material dialog above the current contents of the app,
            // with Material entrance and exit animations, modal barrier color,
            // and modal barrier behavior (dialog is dismissible with a tap on the barrier).
            return Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                actions: <Widget>[
                  IconSlideAction(
                    caption: 'Edit',
                    color: Colors.blue,
                    icon: Icons.edit,
                    onTap: () {

                      navigateToPage(item,context);
                    },
                  ),
                  // IconSlideAction(
                  //   caption: 'Share',
                  //   color: Colors.indigo,
                  //   icon: Icons.share,
                  //   onTap: () {},
                  // ),
                ],
                secondaryActions: <Widget>[
                  IconSlideAction(
                    caption: 'Delete',
                    color: Colors.red,
                    icon: Icons.delete,
                    onTap: () {
                        showAlertDialog(context, () async {
                          setState(() {
                            noteDao.deleteNote(item);
                            todolist.remove(item);
                            // List<Note> notes = await noteDao.findAllNotes();
                            Navigator.of(context).pop();
                          });


                      });

                    },
                  ),
                ],
                key: Key(item.noteTitle),
                child: Card(
                  child: ListTile(
                    title: Text(todolist[index].noteTitle,
                        style: TextStyle(color: Colors.black, fontSize: 25)),
                    subtitle: Text(todolist[index].description,
                        style: TextStyle(color: Colors.red, fontSize: 20)),
                    trailing: Checkbox(
                      checkColor: Colors.greenAccent,
                      activeColor: Colors.red,
                      value: todolist[index].done,
                      onChanged: (bool value)async {
                        setState(() {
                          todolist[index].done = value;
                        });
                      await  noteDao.updateNote(todolist[index]);
                        // List<Note> notes = await noteDao.findAllNotes();
                      },
                    ),
                  ),
                ));
          }),
    );
  }

  void navigateToPage(Note note,BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute<Note>(builder: (context) => TodoWidget(note: note)),
    ).then((value) async  {
      setState(() async {
        // if(value!=null)
        // todolist.add(value);
        await refreshDatebase();
      });

    });
  }
  showAlertDialog(BuildContext context,VoidCallback deleteCallback) {

    // Create button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: deleteCallback,
    );
    Widget cancel = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Are you sure you want to delete this note"),
      actions: [
        okButton,cancel
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
// class MyAlert extends StatelessWidget {
//   // NoteDao noteDao;
//   // Note note;
//   VoidCallback deleteCallback;
//   MyAlert({Key key,this.deleteCallback}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(20.0),
//       child: RaisedButton(
//         child: Text('Show alert'),
//         onPressed: () {
//           showAlertDialog(context,deleteCallback);
//         },
//       ),
//     );
//   }
//
// }


class todos {
  String title;
  String description;
  bool choice;

  todos(this.title, this.description, this.choice);
}
